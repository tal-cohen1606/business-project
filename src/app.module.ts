import { IngredientsModule } from "./ingredients/ingredients.module";
import { ProductsModule } from "./products/products.module";
import { OrdersModule } from "./orders/orders.module";
import { ScheduleModule } from "@nestjs/schedule";
import * as connectionOptions from "./ormconfig";
import { AppController } from "./app.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AppService } from "./app.service";
import { Module } from "@nestjs/common";

@Module({
  imports: [
    IngredientsModule,
    OrdersModule,
    ProductsModule,
    TypeOrmModule.forRoot(connectionOptions),
    ScheduleModule.forRoot(),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
