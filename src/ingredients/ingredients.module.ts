import { IngredientsController } from "./ingredients.controller";
import { IngredientsService } from "./ingredients.service";
import { IngredientSchema } from "./ingredient.schema";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Module } from "@nestjs/common";

@Module({
  imports: [TypeOrmModule.forFeature([IngredientSchema])],
  exports: [TypeOrmModule],
  controllers: [IngredientsController],
  providers: [IngredientsService],
})
export class IngredientsModule {}
