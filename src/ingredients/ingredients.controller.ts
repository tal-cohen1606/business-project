import { Body, Controller, Delete, Get, Param, Patch } from "@nestjs/common";
import { IngredientsService } from "./ingredients.service";

@Controller("ingredients")
export class IngredientsController {
  constructor(private readonly ingredientsService: IngredientsService) {}

  @Get()
  getAllIngredients() {
    return this.ingredientsService.findAll();
  }

  @Get(":id")
  getIngredient(@Param("id") id: number) {
    return this.ingredientsService.findOne(id);
  }

  @Patch(":_id")
  updateIngredientAmount(
    @Param("_id") id: number,
    @Body("amount") amount: number
  ) {
    return this.ingredientsService.updateIngredientAmount(id, amount);
  }

  @Delete(":_id")
  removeUser(@Param("_id") id: number) {
    return this.ingredientsService.remove(id);
  }
}
