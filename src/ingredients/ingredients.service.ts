import { InjectRepository } from "@nestjs/typeorm";
import { Repository, UpdateResult } from "typeorm";
import { Ingredient } from "./ingredient.entity";
import { Injectable } from "@nestjs/common";

@Injectable()
export class IngredientsService {
  constructor(
    @InjectRepository(Ingredient)
    private ingredientsRepository: Repository<Ingredient>
  ) {}

  findAll(): Promise<Ingredient[]> {
    return this.ingredientsRepository.find();
  }

  findOne(id: number): Promise<Ingredient> {
    return this.ingredientsRepository.findOne(id);
  }

  remove(id: number): Promise<{}> {
    return this.ingredientsRepository.delete(id);
  }

  updateIngredientAmount(id: number, amount: number): Promise<UpdateResult> {
    this.ingredientsRepository
      .findOne({
        select: ["amount"],
        where: { id },
      })
      .then();

    return this.ingredientsRepository.update({ id }, { amount: amount });
  }
}
