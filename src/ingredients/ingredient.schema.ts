import { Ingredient } from "./ingredient.entity";
import { EntitySchema } from "typeorm";

export const IngredientSchema = new EntitySchema<Ingredient>({
  name: "Ingredient",
  target: Ingredient,
  columns: {
    id: {
      type: Number,
      primary: true,
      generated: true,
    },
    name: {
      type: String,
    },
    description: {
      type: String,
    },
    amount: {
      type: Number,
    },
    time: {
      type: Number,
    },
  },
});
