import { EntitySchema } from "typeorm";
import { Order } from "./order.entity";

export const OrderSchema = new EntitySchema<Order>({
  name: "Order",
  target: Order,
  columns: {
    id: {
      type: Number,
      primary: true,
      generated: true,
    },
    orderName: {
      type: String,
    },
    orderAddress: {
      type: String,
    },
    createdAt: {
      type: Number,
    },
    completedAt: {
      type: Number,
    },
    status: {
      type: "enum",
    },
  },
});
