import { OrdersController } from "./orders.controller";
import { OrdersService } from "./orders.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { OrderSchema } from "./order.schema";
import { Module } from "@nestjs/common";

@Module({
  imports: [TypeOrmModule.forFeature([OrderSchema])],
  controllers: [OrdersController],
  providers: [OrdersService],
})
export class OrdersModule {}
