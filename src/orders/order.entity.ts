import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Product } from "../products/product.entity";
import { OrderStatus } from "./enums/order.enum";

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  orderAddress: string;

  @Column()
  orderName: string;

  @Column()
  status: OrderStatus;

  @Column()
  createdAt: number;

  @Column()
  completedAt: number;

  @ManyToMany(() => Product)
  @JoinTable()
  products: Product[];
}
