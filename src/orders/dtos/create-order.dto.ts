import { IsArray, IsEnum, IsString } from "class-validator";
import { Product } from "../../products/product.entity";
import { OrderStatus } from "../enums/order.enum";

export class CreateOrderDto {
  @IsString()
  orderAddress: string;

  @IsString()
  orderName: string;

  @IsEnum(String)
  status: OrderStatus;

  @IsArray()
  products: Product[];
}
