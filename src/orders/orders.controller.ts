import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Res,
} from "@nestjs/common";
import { CreateOrderDto } from "./dtos/create-order.dto";
import { OrdersService } from "./orders.service";
import { Order } from "./order.entity";

@Controller("orders")
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Post()
  addOrder(@Body() createOrder: CreateOrderDto, @Res() res): Promise<Order> {
    return this.ordersService.insertOrder(createOrder, res);
  }

  @Get()
  getAllOrders(): Promise<Order[]> {
    return this.ordersService.findAll();
  }

  @Get(":id")
  getOrder(@Param("id") id: number): Promise<Order> {
    return this.ordersService.findOne(id);
  }

  @Delete(":id")
  removeUser(@Param("id") id: number): Promise<{}> {
    return this.ordersService.remove(id);
  }

  @Patch(":_id")
  updateOrder(
    @Param("_id") id: number,
    @Body("products") products: [],
    @Res() res
  ) {
    return this.ordersService.updateOrder(id, products, res);
  }
}
