import { CreateOrderDto } from "./dtos/create-order.dto";
import { Product } from "../products/product.entity";
import { getRepository, Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { OrderStatus } from "./enums/order.enum";
import { Injectable } from "@nestjs/common";
import { Cron } from "@nestjs/schedule";
import { Order } from "./order.entity";

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>
  ) {}

  async insertOrder(createOrder: CreateOrderDto, res): Promise<Order> {
    const products = await this.getProducts(createOrder);
    const availability = await this.isProductAvailable(createOrder);
    const newOrder = new Order();

    if (!availability) {
      return res.status(500).json({
        message: {
          msg: "You have product with no available ingredients",
          msgError: true,
        },
      });
    }

    newOrder.orderAddress = createOrder.orderAddress;
    newOrder.orderName = createOrder.orderName;
    newOrder.products = products;
    newOrder.createdAt = new Date(Date.now()).getTime() / 1000;
    newOrder.status = OrderStatus.WAITING_TO_KITCHEN;
    newOrder.completedAt = 0;

    return this.ordersRepository.save(newOrder);
  }

  async getProducts(createOrder: CreateOrderDto) {
    const productRepository = getRepository(Product);
    return await Promise.all(
      createOrder.products.map((id) =>
        productRepository.findOne({
          relations: ["ingredients"],
          where: { id },
        })
      )
    );
  }

  async isProductAvailable(createOrder: CreateOrderDto): Promise<boolean> {
    const products = await this.getProducts(createOrder);
    let isProductAvailable = true;

    products.map((product) => {
      if (!product.available) {
        isProductAvailable = false;
      } else {
        isProductAvailable = true;
      }
    });

    return isProductAvailable;
  }

  getProcessStatus() {
    let taskRunning = false;

    return taskRunning;
  }

  @Cron("* * * * * *", {
    name: "orders",
  })
  async createOrders() {
    this.getProcessStatus();
    const sortedOrders = await this.ordersRepository.find({
      relations: ["products"],
      order: {
        createdAt: "ASC",
      },
      where: { status: "WAITING_TO_KITCHEN" },
    });
    for (let i = 0; i < sortedOrders.length; i++) {
      const order = sortedOrders[i];
      const time = order.products.reduce(
        (acc: number, product) => acc + product.cookingTime,
        0
      );
      const completedAt = new Date(Date.now()).getTime() / 1000;
      const id = order.id;

      setTimeout(() => {
        !this.getProcessStatus();
        this.ordersRepository.update(
          { id },
          { status: OrderStatus.READY_FOR_DELIVERY, completedAt: completedAt }
        );
      }, time);

      if (!this.getProcessStatus()) {
        return;
      }
    }
  }

  findAll(): Promise<Order[]> {
    return this.ordersRepository.find({
      relations: ["products"],
    });
  }

  findOne(id: number): Promise<Order> {
    const _order = this.ordersRepository.findOne({
      relations: ["products"],
      where: { id },
    });

    return _order;
  }

  remove(id: number): Promise<{}> {
    return this.ordersRepository.delete(id);
  }

  async updateOrder(id: number, products: [], res) {
    const order = await this.ordersRepository.findOne({
      relations: ["products"],
      where: { id },
    });
    const productRepository = getRepository(Product);
    const _products = await Promise.all(
      products.map((id) =>
        productRepository.findOne({
          where: { id },
        })
      )
    );
 /*   const availability = await this.isProductAvailable(order);


    if (!availability) {
      return res.status(500).json({
        message: {
          msg: "You want to change order with no available products",
          msgError: true,
        },
      });
    }else{
      order.products = _products;

      return this.ordersRepository.save(order);
    }*/
    order.products = _products;

    return this.ordersRepository.save(order);
  }
}
