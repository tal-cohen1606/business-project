import { CreateProductDto } from "./create-product.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Injectable } from "@nestjs/common";
import { Product } from "./product.entity";
import { Repository } from "typeorm";

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>
  ) {}

  insertProduct(createProduct: CreateProductDto): Promise<Product> {
    const newProduct = new Product();

    newProduct.name = createProduct.name;
    newProduct.productDetail = createProduct.productDetail;
    newProduct.price = createProduct.price;

    return this.productsRepository.save(newProduct);
  }

  findAll(): Promise<Product[]> {
    return this.productsRepository.find({
      relations: ["ingredients"],
    });
  }

  async findOne(id: number): Promise<Product> {
    const _product = this.productsRepository.findOne({
      relations: ["ingredients"],
      where: { id },
    });

    await this.updateProductStatus(id);
    await this.calcCookingTime(id);

    return _product;
  }

  remove(id: number): Promise<{}> {
    return this.productsRepository.delete(id);
  }

  async calcCookingTime(id: number) {
    const _product = await this.productsRepository.findOne({
      relations: ["ingredients"],
      where: { id },
    });

    let cookingTime = _product.ingredients.reduce(function (acc, ingredient) {
      return acc + ingredient.time;
    }, 0);

    await this.productsRepository.update({ id }, { cookingTime: cookingTime });

    return cookingTime;
  }

  async updateProductStatus(id: number) {
    const _product = await this.productsRepository.findOne({
      relations: ["ingredients"],
      where: { id },
    });

    let isValidAmount = true;
    _product.ingredients.map((ingredient) => {
      if (ingredient.amount === 0) {
        isValidAmount = false;
      }
    });

    if (!isValidAmount) {
      await this.productsRepository.update({}, { available: false });
    } else {
      await this.productsRepository.update({}, { available: true });
    }

    return _product;
  }
}
