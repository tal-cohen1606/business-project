import { Product } from "./product.entity";
import { EntitySchema } from "typeorm";

export const ProductSchema = new EntitySchema<Product>({
  name: "Product",
  target: Product,
  columns: {
    id: {
      type: Number,
      primary: true,
      generated: true,
    },
    name: {
      type: String,
    },
    productDetail: {
      type: String,
    },
    price: {
      type: Number,
    },
    available: {
      type: Boolean,
    },
    cookingTime: {
      type: Number,
    },
  },
});
