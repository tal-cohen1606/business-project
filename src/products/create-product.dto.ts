import { IsBoolean, IsNumber, IsString } from "class-validator";

export class CreateProductDto {
  @IsString()
  name: string;

  @IsString()
  productDetail: string;

  @IsNumber()
  price: number;

  @IsBoolean()
  available: boolean;
}
