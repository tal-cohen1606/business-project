import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Ingredient } from "../ingredients/ingredient.entity";
import { Order } from "../orders/order.entity";

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  productDetail: string;

  @Column()
  price: number;

  @Column()
  available: boolean;

  @Column()
  cookingTime: number;

  @ManyToMany(() => Ingredient)
  @JoinTable()
  ingredients: Ingredient[];

  @ManyToMany((type) => Order)
  order: Order[];
}
