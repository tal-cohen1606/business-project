import { Body, Controller, Delete, Get, Param, Post } from "@nestjs/common";
import { CreateProductDto } from "./create-product.dto";
import { ProductsService } from "./products.service";
import { Product } from "./product.entity";

@Controller("products")
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post()
  addProduct(@Body() createProduct: CreateProductDto): Promise<Product> {
    return this.productsService.insertProduct(createProduct);
  }

  @Get()
  getAllProduct(): Promise<Product[]> {
    return this.productsService.findAll();
  }

  @Get(":id")
  getProduct(@Param("id") id: number): Promise<Product> {
    return this.productsService.findOne(id);
  }

  @Delete(":_id")
  removeProduct(@Param("_id") id: number): Promise<{}> {
    return this.productsService.remove(id);
  }
}
