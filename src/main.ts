import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
const cors = require("cors");

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const corsOptions = {
    origin: [
      "http://127.0.0.1:3000",
      "http://localhost:3000",
    ],
    credentials: true,
  };

  app.use(cors(corsOptions));

  await app.listen(3030);
}

bootstrap();
