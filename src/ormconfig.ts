import { Ingredient } from "./ingredients/ingredient.entity";
import { Product } from "./products/product.entity";
import { Order } from "./orders/order.entity";
import { ConnectionOptions } from "typeorm";
import { join } from "path";

const config = {
  host: "localhost",
  user: "tal",
  password: "password",
  database: "business_project",
  port: 3306,
};

const connectionOptions: ConnectionOptions = {
  type: "mysql",
  host: config.host,
  port: config.port,
  username: config.user,
  password: config.password,
  database: config.database,
  entities: [Ingredient, Product, Order],
  synchronize: true,
  dropSchema: false,
  migrationsRun: true,
  migrations: [join(__dirname, "migrations/*{.ts,.js}")],
  cli: {
    migrationsDir: "src/migrations",
  },
};
export = connectionOptions;
